﻿using System;

namespace Migrations
{
    public sealed class TransactionalMigrationAttribute : MigrationAttribute
    {
        public TransactionalMigrationAttribute(Type migrationType)
            : base(migrationType, true)
        {
        }
    }
}