﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using FluentMigrator;

namespace Migrations
{
    public abstract class Migration : ForwardOnlyMigration
    {
        public const string Prefix = "Migration";
        public const char DescriptionDivider = '_';

        public static readonly Regex Regex = new Regex(
            $"(?(^{Prefix}\\d{{1,18}}{DescriptionDivider}(\\p{{Lu}}|\\p{{Ll}}){{1,1024}}$)(^\\w{{1,1024}}$))",
            RegexOptions.CultureInvariant | RegexOptions.Singleline | RegexOptions.Compiled);

        protected Migration()
        {
            EnsureValid();
        }

        public string Name => GetType().GetName();
        public long Index => GetType().GetIndex();

        private void EnsureValid()
        {
            if (!Regex.IsMatch(Name))
                throw new ArgumentException($"Invalid {nameof(Migration)} {nameof(Name)}: {Name}", nameof(Name));
        }

        public sealed override void Up()
        {
            Execute.EmbeddedScript($"{Name}.sql");
        }
    }
}