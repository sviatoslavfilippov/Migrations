﻿using System;
using System.Linq;

namespace Migrations
{
    public static class MigrationExtensions
    {
        public static string GetName(this Type migrationType)
        {
            return migrationType.Name;
        }

        public static long GetIndex(this Type migrationType)
        {
            var migrationName = migrationType.Name;
            var migrationIndexString = string.Concat(migrationName
                .Skip(Migration.Prefix.Length)
                .TakeWhile(c => c != Migration.DescriptionDivider)
            );
            var migrationIndex = long.Parse(migrationIndexString);
            return migrationIndex;
        }
    }
}
