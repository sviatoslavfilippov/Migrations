﻿using FluentMigrator.Runner.VersionTableInfo;

namespace Migrations
{
#pragma warning disable 618
    [VersionTableMetaData]
    public class MigrationsVersionTable : DefaultVersionTableMetaData
    {
        public override string TableName => "Migrations";
        public override string ColumnName => "Index";
        public override string UniqueIndexName => $"CLIX_{ColumnName}";
        public override string DescriptionColumnName => "Name";
#pragma warning restore 618
    }
}