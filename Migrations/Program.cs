﻿using System;
using FluentMigrator.Runner;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Migrations
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = "Data Source=localhost\\SQLEXPRESS; Integrated Security=SSPI; Initial Catalog=Test;";

            var serviceProvider = CreateServices(connectionString);

            using (var scope = serviceProvider.CreateScope())
                UpdateDatabase(scope.ServiceProvider);

            Console.ReadLine();
        }

        private static IServiceProvider CreateServices(string connectionString)
        {
            return new ServiceCollection()
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    .AddSqlServer2016()
                    .WithGlobalConnectionString(connectionString)
                    .ScanIn(typeof(Migration).Assembly).For.All())
                .AddLogging(lb => lb.AddFluentMigratorConsole().SetMinimumLevel(LogLevel.Trace))
                .BuildServiceProvider(validateScopes: false);
        }
        
        private static void UpdateDatabase(IServiceProvider serviceProvider)
        {
            var runner = serviceProvider.GetRequiredService<IMigrationRunner>();

            runner.ListMigrations();
            runner.ValidateVersionOrder();
            runner.MigrateUp();
        }
    }
}