﻿using System;
using FluentMigrator;

namespace Migrations
{
    public abstract class MigrationAttribute : FluentMigrator.MigrationAttribute
    {
        protected MigrationAttribute(Type migrationType, bool useTransaction)
            : base(migrationType.GetIndex(),
                GetTransactionBehavior(useTransaction),
                migrationType.GetName())
        {
        }

        private static TransactionBehavior GetTransactionBehavior(bool useTransaction) =>
            useTransaction ? TransactionBehavior.Default : TransactionBehavior.None;
    }
}