﻿using System;

namespace Migrations
{
    public sealed class UntransactionalMigrationAttribute : MigrationAttribute
    {
        public UntransactionalMigrationAttribute(Type migrationType)
            : base(migrationType, false)
        {
        }
    }
}